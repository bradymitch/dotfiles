# Open circleci build list for repo and branch
function ci() {
    local WORKING_DIR
    local URL_PREFIX
    local REMOTE
    local REPO
    local BRANCH

    if [[ -n "$1" ]]; then
        WORKING_DIR="$CODE_DIR/$1"
    else
        WORKING_DIR="$(pwd)"
    fi

    URL_PREFIX="https://circleci.com/gh"
    REMOTE=$(git -C "$WORKING_DIR" remote get-url origin)

    if [[ $REMOTE == git@* ]]; then
        REPO=$(echo "$REMOTE" | cut -d":" -f2 | cut -d "." -f1)
    elif [[ $REMOTE == http* ]]; then
        REPO=$(echo "$REMOTE" | cut -d"/" -f4- | cut -d "." -f1)
    fi

    BRANCH=$(git -C "$WORKING_DIR" symbolic-ref --short HEAD)

    python3 -m webbrowser "$URL_PREFIX/$REPO/tree/$BRANCH" &>/dev/null
}