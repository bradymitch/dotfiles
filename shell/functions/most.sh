# Show directories using the most disk space
function most() {
    local WORKING_DIR="${1-$(pwd)}"
    local NUM_DIRS="${2-10}"

    du -hx -d 1 "$WORKING_DIR" | sort -rh | head -n "$NUM_DIRS"
}