# Open github url for repo
function gh() {
    local WORKING_DIR="${1-$(pwd)}"
    local URL

    URL=$(git -C "$WORKING_DIR" remote get-url origin | sed 's#git@github.com:#https://github.com/#')

    if [[ -z "$URL" ]]; then
        echo "Unable to get github URL for git repo, is $WORKING_DIR a git repo?"
        return
    fi

    python3 -m webbrowser "$URL" &>/dev/null
}