# Start an HTTP server from a directory and open browser to it. Can pass port as argument.
function server() {
    local PORT="${1:-8000}"
    sleep 1 && python3 -m webbrowser "http://localhost:${PORT}/" &
    python3 -m http.server "$PORT"
}
