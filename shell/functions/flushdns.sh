# Flush the dns cache
function flushdns() {
    OS=$(uname)

    if [[ "$OS" == 'Linux' ]]; then
        sudo systemd-resolve --flush-caches
    elif [[ "$OS" == 'Darwin' ]]; then
        sudo killall -HUP mDNSResponder
    else
        echo "Unsupported OS: $OS"
    fi
}