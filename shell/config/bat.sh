# -----------------------------------------------------
# bat: A cat(1) clone with wings.
# https://github.com/sharkdp/bat
# -----------------------------------------------------
if command -v bat &>/dev/null; then
    export BAT_THEME="OneHalfDark"
    alias batlog='bat --paging=never -l log -n'
    alias cat='bat'
fi