# -----------------------------------------------------
# vim
# https://www.vim.org
# -----------------------------------------------------
if command -v vim &>/dev/null; then
    # Make vim the default editor
    export EDITOR="vim"
    export VISUAL="vim"
fi