# -----------------------------------------------------
# lab: interact with repositories on GitLab
# https://github.com/zaquestion/lab
# -----------------------------------------------------
if command -v lab &>/dev/null; then
    # Lab will ask for input if these are not set and
    # cause the shell to hang indefinitely if not.
    if [[ -n "$LAB_CORE_HOST" && -n "$LAB_CORE_USER" && -n "$LAB_CORE_TOKEN" ]]; then
        # shellcheck disable=SC1090
        source <(lab completion bash)
    fi
fi