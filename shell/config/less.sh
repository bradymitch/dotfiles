
# -----------------------------------------------------
# less
# -----------------------------------------------------
# Make colors work correctly in less
export LESS=-R

# If zless is available, use it instead of less to enable viewing compressed files.
if command -v zless &>/dev/null; then
    alias less='zless'
fi
