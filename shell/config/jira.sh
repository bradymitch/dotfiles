# -----------------------------------------------------
# jira: Simple command line client for Atlassian's Jira service written in Go.
# https://github.com/go-jira/go-jira
# -----------------------------------------------------
if command -v jira &>/dev/null; then
    eval "$(jira --completion-script-bash)"
fi