# Only add to $PATH if directory exists and not already in $PATH.
# By default goes at the beginning of $PATH
# Pass 'after' as second param to put at the end of $PATH
if ! [[ $(type -t pathmunge 2>/dev/null) == "function" ]]; then
    pathmunge () {
        if [[ -d "$1" ]]; then
            if ! echo "$PATH" | /bin/grep -Eq "(^|:)$1($|:)" ; then
                if [ "$2" = "after" ] ; then
                    PATH="$PATH:$1"
                else
                    PATH="$1:$PATH"
                fi
            fi
        fi
    }
fi

# Easier view of $PATH
if ! [[ $(type -t path 2>/dev/null) == "alias" ]]; then
    alias path='echo -e ${PATH//:/\\n}'
fi

# Add some directories to $PATH
pathmunge "$HOME/bin"
pathmunge "$HOME/.local/bin"
pathmunge "$HOME/.asdf/shims"
pathmunge "$HOME/.asdf/bin"