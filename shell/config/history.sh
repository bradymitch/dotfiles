# -----------------------------------------------------
# history settings
# -----------------------------------------------------

# Append to the Bash history file, rather than overwriting it
shopt -s histappend

# Store multiline commands as single entries in history file
shopt -s cmdhist

# Allow re-editing of a history command substitution, if the previous run failed
shopt -s histreedit

# Increase size of history
export HISTSIZE=10000
export HISTFILESIZE=$HISTSIZE

# Don't add items that begin with a space or duplicate commands to history
export HISTCONTROL=erasedups:ignoreboth

# Make some commands not show up in history
export HISTIGNORE="ls:cd:cd -:pwd:exit:date"

# puts full date and time in history records.
export HISTTIMEFORMAT="[%FT%T] "

# mem/file sync
export PROMPT_COMMAND="history -a;${PROMPT_COMMAND}"