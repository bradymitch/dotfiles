#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
if [[ ! -z ${DEBUG_MODE+x} ]]; then
    set -o xtrace
    set -o functrace
fi

# Use LibreOffice and pandoc to convert from wpd to rst. Also cleans up filename by removing whitespace, lowercasing entire name and extension.

REQUIREMENTS_MET=TRUE
REQUIREMENTS="soffice pandoc rename"
for i in $REQUIREMENTS
do
    if ! command -v "$i" &> /dev/null; then
        REQUIREMENTS_MET=FALSE
    fi
done

if [[ "$REQUIREMENTS_MET" == "FALSE" ]]; then
    echo "Not all requirements are present, $REQUIREMENTS must all be installed"
    exit 1
fi

WORKING_DIR=$(mktemp -d)

function cleanup_before_exit () {
  rm -rf "$WORKING_DIR"
}

trap cleanup_before_exit EXIT

INPUT_FILE="$1"
BASE_FILE_NAME=$(basename -s .wpd -s .WPD "$INPUT_FILE")
DOCX_FILE_NAME="$BASE_FILE_NAME.docx"
OUTPUT_FILE="$BASE_FILE_NAME.rst"

soffice --headless --convert-to docx --outdir "$WORKING_DIR" "$INPUT_FILE" &> /dev/null
pandoc -f docx -t rst -o "$OUTPUT_FILE" "$WORKING_DIR/$DOCX_FILE_NAME"
rename --force --lower-case --sanitize --subst-all "_" "-" "$OUTPUT_FILE"
